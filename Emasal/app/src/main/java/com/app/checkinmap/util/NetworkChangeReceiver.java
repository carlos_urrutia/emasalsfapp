package com.app.checkinmap.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.app.checkinmap.R;
import com.app.checkinmap.db.DatabaseManager;

/**
 * This class listen the change in the network
 * connection
 */

public class NetworkChangeReceiver extends BroadcastReceiver{
    public static final String TAG= NetworkChangeReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        int status = NetworkUtil.getConnectivityStatusString(context);

        if (!"android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if(status==NetworkUtil.NETWORK_STATUS_NOT_CONNECTED){
                Log.e(TAG, "Sin conexion a Internet");
                 /*Here we track the user action*/
                DatabaseManager.getInstance().saveUserAction(context.getString(R.string.wifi_disable));
            }else{
                Log.e(TAG, "Conexion a Internet establecida");
                 /*Here we track the user action*/
                DatabaseManager.getInstance().saveUserAction(context.getString(R.string.wifi_enable));
            }

        }
    }
}
