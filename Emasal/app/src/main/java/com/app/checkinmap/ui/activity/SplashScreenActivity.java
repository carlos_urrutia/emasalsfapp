package com.app.checkinmap.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.app.checkinmap.R;
import com.app.checkinmap.util.ApiManager;
import com.app.checkinmap.util.AppStatus;
import com.app.checkinmap.util.NetworkUtilsTask;
import com.app.checkinmap.util.OnNetworkListener;
import com.app.checkinmap.util.Utility;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.ui.SalesforceActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NetworkUtilsTask(this, new OnNetworkListener() {
            @Override
            public void onNetwork(boolean success, String message) {
                if(success){
                    startActivity(SalesForceLoginActivity.getIntent(getApplicationContext()));
                }else{
                    startActivity(NoInternetActivity.getIntent(getApplicationContext()));
                }
                finish();
            }
        }).execute();
    }

}
