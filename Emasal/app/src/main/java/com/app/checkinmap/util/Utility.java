package com.app.checkinmap.util;

import android.os.Environment;
import android.util.Log;

import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.RestClient;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class help us encapsulate
 * all the common method in the application
 */

public class Utility {
    public static final String      BACKUP_DIRECTORY="EmasalApp"+File.separator+"Backup";
    public static final String TAG="EMASAL";
    private static RestClient  mRestClient;
    private static String      mUserProfileId;
    private static Roles       mUserRole;
    private static String      mUserProfileName;
    private static String      mUserCountry;
    private static int         mRadioCheckIn;
    private static int         mIntervalSeconds;

    public enum Roles{
        SELLER,
        MANAGER,
        TECHNICAL,
        CUSTOMER_SERVICE,
        TECHNICAL_COORDINATOR,
        OTHER
    }

    /**
     * This method help us to get the current
     * date with hours
     */
    public static String getCurrentDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getCurrentSalesforceDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000Z");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * This method set the Rest client instance from the login
     */
    public static void setRestClient(RestClient restClient){
        mRestClient = restClient;
    }

    /**
     * This method set the Rest client instance from the login
     */
    public static RestClient getRestClient(){
        return mRestClient;
    }


    /**
     * This method set the user profile id like a global variable
     */
    public static void setUserProfileId(String userProfileId){
        mUserProfileId = userProfileId;
        /*Here we check if the user is a seller*/
        switch (userProfileId){
            case "00e6A000000IRoOQAW":
                setUserRole(Roles.SELLER);
                break;
            case "00e6A000000IRoEQAW":
                setUserRole(Roles.TECHNICAL);
                break;
            case "00e6A000000IRoTQAW":
                setUserRole(Roles.CUSTOMER_SERVICE);
                break;
            case "00e6A000000IRoJQAW":
                setUserRole(Roles.TECHNICAL_COORDINATOR);
                break;
            case "00e6A000000IRnzQAG":
                setUserRole(Roles.MANAGER);
                break;
            default:
                setUserRole(Roles.OTHER);
                break;
        }
    }

    /**
     * This method get the user profile id like
     */
    public static String getUserProfileId(){
        return mUserProfileId ;
    }

    /**
     * This method set the user type*/
    public static void setUserRole(Roles userRole){
        mUserRole = userRole;
    }

    /**
     * This method get the user type
     */
    public static Roles getUserRole(){
        return mUserRole ;
    }


    /**
     * This method get the user profile name
     */
    public static String getUserProfileName(){
        return mUserProfileName ;
    }

    /**
     * This method set the user profile name*/
    public static void setUserProfileName(String userProfileName){
        mUserProfileName = userProfileName;
    }


    /**
     * This method help us to
     * show the large JSON
     */
    public static void logLargeString(String str) {
        if(str.length() > 3000) {
            Log.i(TAG, str.substring(0, 3000));
            logLargeString(str.substring(3000));
        } else {
            Log.i(TAG, str); // continuation
        }
    }

    /**
     * This method get the user country
     */
    public static String getUserCountry(){
        return mUserCountry ;
    }

    /**
     * This method set the user country*/
    public static void setUserCountry(String userCountry){
        mUserCountry = userCountry;
    }

    /**
     * Get radio
     * @return
     */
    public static int getRadioCheckIn() {
        return  mRadioCheckIn;
    }

    /**
     * Set radio
     * @param radioCheckIn
     */
    public static void setRadioCheckIn(int radioCheckIn){
        mRadioCheckIn = radioCheckIn;
    }

    /**
     * Get refresh location interval in seconds
     * @return
     */
    public  static int getIntervalSeconds(){
        return mIntervalSeconds;
    }

    /**
     * Set refresh location interval in seconds
     * @param intervalSeconds
     */
    public static void setIntervalSeconds(int intervalSeconds){
        mIntervalSeconds = intervalSeconds;
    }

    /**
     * This method help us to get the current
     * date with hours for route name
     */
    public static String getDateForName(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * This method help us to get the current
     * date with hours for route name
     */
    public static String getDateForNameSimple(){
        DateFormat dateFormat = new SimpleDateFormat("ddMMyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * This method help us to get the current
     * date with hours for route search
     */
    public static String getDateForSearch(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * This method help us to get the time in hour
     * for the visit
     */
    public static String getDurationInHours(String dateStart,String dateFinish){
        String time ="0 horas 0 min";

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date d1;
        Date d2;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateFinish);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

           // long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
           // long diffDays = diff / (24 * 60 * 60 * 1000);

            time = "";
            time = diffHours+" horas "+diffMinutes+" min";

        } catch (Exception e) {
            e.printStackTrace();
        }

        return time;
    }


    /**
     * This method help us to get the time in hour
     * for the visit
     */
    public static double getDurationInHoursNumber(String dateStart,String dateFinish){
        double totalTime= 0.000;

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date d1;
        Date d2;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateFinish);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            // long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            // long diffDays = diff / (24 * 60 * 60 * 1000);

            totalTime = diffHours;

            totalTime = totalTime + (diffMinutes/60.0);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return totalTime;
    }


    public static String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z-áéíóú])([a-z-áéíóú]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    public static String truncate(String value, int length) {
        // Ensure String length is longer than requested size.
        if (value.length() > length) {
            return value.substring(0, length);
        } else {
            return value;
        }
    }

    public static void checkSFSession() {
        if (SalesforceSDKManager.getInstance().getUserAccountManager().getCurrentUser() != null) {
            Log.d(TAG, "Refresh Token");
            SalesforceSDKManager.getInstance().getUserAccountManager().getCurrentUser().getRefreshToken();
        }
    }

    /**
     * This method help us to create a txt file in order
     * to prepare the data to make a backup by user in Sales
     * Force
     */
    public static File createBackUpFile(String type) throws IOException {
        //Here we define the backup file
        File backupFile=null;

        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "backup_"+type+"_"+Utility.getRestClient().getClientInfo().username+"_"+timeStamp+".csv";

        //create a file to write bitmap data
        File directory= new File(Environment.getExternalStorageDirectory(),BACKUP_DIRECTORY);

        //Here we verify if exist the directory
        if(!directory.exists()){
            directory.mkdirs();
        }

        //create a file to write bitmap data
        backupFile= new File(directory.getAbsolutePath(), fileName);
        if(!backupFile.exists()){
            backupFile.createNewFile();
        }

        return backupFile;
    }

    /**
     * This method help us to encode our file before
     * to make the salesforce request
     */
   public static String encodeFileToBase64(File file){
       byte[] encoded = new byte[0];
       try {
           encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
       } catch (IOException e) {
           e.printStackTrace();
       }
       return new String(encoded, StandardCharsets.UTF_8);
    }

    /**
     * This method help us to get the current
     * date with milliseconds
     */
    public static String getCurrentDateWithTime(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
